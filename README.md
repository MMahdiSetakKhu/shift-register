[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Universal Shift Register
An N-bit Shift register with enable, parallel load, asynchronous reset and the ability to shift to the right and left using two serial inputs.

## Test
All the features have been tested within the "Tests" file.

## Sample TestBench Result
```
# change the direction to <- 1
# xxx1
# xx11
# x111
# 1111
# change the direction to <- 0
# 1110
# 1100
# 1000
# 0000
# change the direction to 1 ->
# 1000
# 1100
# 1110
# load to 0000
# 0000
# load to 0110
# 0110
# 0110
# reset
# 0000
# 0000
# change the direction to 1 ->
# 1000
# 1100
# disabled
# 1100
# 1100
```

## License
MIT License

Copyright (c) 2020 M.Mahdi Setak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
